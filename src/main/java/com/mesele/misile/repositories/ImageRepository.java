package com.mesele.misile;

import java.awt.Image;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository{

	Image findByName(String text);

	Image findFirstByName(String text);

}
