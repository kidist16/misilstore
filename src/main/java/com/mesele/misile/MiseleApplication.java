package com.mesele.misile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiseleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiseleApplication.class, args);
	}

}

